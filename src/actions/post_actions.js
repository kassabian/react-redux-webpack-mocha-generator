import axios from 'axios';
const BASE_URL = '//jsonplaceholder.typicode.com/';

export const GET_ALL_POSTS = 'GET_ALL_POSTS';
export const GET_USER_POSTS = 'GET_USER_POSTS';
export const GET_POST_BY_ID = 'GET_POST_BY_ID';
export const GET_POST_COMMENTS = 'GET_POST_COMMENTS';
export const ADD_POST = 'ADD_POST';
export const DELETE_POST = 'DELETE_POST';

export function getAllPosts() {
  const request = axios.get(`${BASE_URL}posts`);

  return {
    type: GET_ALL_POSTS,
    payload: request
  };
}

export function getUserPosts(uid) {
  const request = axios.get(`${BASE_URL}posts`, {
    params: {
      userId: uid
    }
  });

  return {
    type: GET_USER_POSTS,
    payload: request
  };
}

export function getPostById(postId) {
  const request = axios.get(`${BASE_URL}posts/${postId}`);

  return {
    type: GET_POST_BY_ID,
    payload: request
  };
}

export function getPostComments(postId) {
  const request = axios.get(`${BASE_URL}posts/${postId}/comments`);

  return {
    type: GET_POST_COMMENTS,
    payload: request
  };
}

export function addPost(post) {
  post.userId = 1;
  const request = axios.post(`${BASE_URL}posts`,
    {
      data: post
    },
    {
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }
  );

  return {
    type: ADD_POST,
    payload: request
  }
}

export function deletePost(postId) {
  const request = axios.delete(`${BASE_URL}posts/${postId}`);

  return {
    type: DELETE_POST,
    payload: request
  };
}
