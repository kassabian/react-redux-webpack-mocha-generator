import axios from 'axios';
const BASE_URL = '//jsonplaceholder.typicode.com/';

export const GET_ALL_USERS = 'GET_ALL_USERS';
export const GET_USER_INFO = 'GET_USER_INFO';


export function getAllUsers() {
  const request = axios.get(`${BASE_URL}users`);

  return {
    type: GET_ALL_USERS,
    payload: request
  };
}

export function getUserInfo(uid) {
  const request = axios.get(`${BASE_URL}users/${uid}`);

  return {
    type: GET_USER_INFO,
    payload: request
  };
}
