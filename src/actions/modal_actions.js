export const SHOW_MODAL = 'SHOW_MODAL';
export const HIDE_MODAL = 'HIDE_MODAL';
export const MODAL_TYPES = {
  ADD_POST_FORM: 'ADD_POST_FORM'
}

export function showModal(type = 'default') {
  return {
    type: SHOW_MODAL,
    payload: {type}
  }
}

export function hideModal() {
  return {
    type: HIDE_MODAL,
    payload: null
  }
}
