export const SHOW_TOASTR = 'SHOW_TOASTR';
export const HIDE_TOASTR = 'HIDE_TOASTR';

export function showToastr(config) {
  return {
    type: SHOW_TOASTR,
    payload: {
      type: config.type,
      title: config.title,
      body: config.body
    }
  };
}

export function hideToastr() {
  return {
    type: HIDE_TOASTR,
    payload: {
      type: null,
      title: null,
      body: null
    }
  };
}
