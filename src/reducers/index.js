import {combineReducers} from 'redux';
import {reducer as formReducer} from 'redux-form';
import userReducer from './user_reducer';
import postReducer from './post_reducer';
import ktoastrReducer from './ktoastr_reducer';
import modalReducer from './modal_reducer';

const rootReducer = combineReducers({
  users: userReducer,
  posts: postReducer,
  form: formReducer,
  ktoastr: ktoastrReducer,
  modal: modalReducer
});

export default rootReducer;
