import {SHOW_MODAL, HIDE_MODAL} from '../actions/modal_actions';

const INITIAL_STATE = {visible: false, type: null};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SHOW_MODAL:
      return Object.assign({}, state, {
        visible: true,
        type: action.payload.type
      });
    case HIDE_MODAL:
      return Object.assign({}, state, {
        visible: false,
        type: null
      });
    default:
      return state;
  }
}
