import {getUsers, GET_ALL_USERS, getUserInfo, GET_USER_INFO} from '../actions/user_actions';

const INITIAL_STATE = {all: [], selectedUser: null};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case GET_ALL_USERS:
      return Object.assign({}, state, {
        all: action.payload.data,
        selectedUser: null
      });
    case GET_USER_INFO:
      return Object.assign({}, state, {
        selectedUser: action.payload.data
      });
    default:
      return state;
  }
}
