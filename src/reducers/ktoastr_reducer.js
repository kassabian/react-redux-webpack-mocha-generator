import {SHOW_TOASTR, HIDE_TOASTR} from '../actions/ktoastr_actions';

const INITIAL_STATE = {
  visible: false,
  config: {
    type: null,
    title: null,
    body: null
  }
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SHOW_TOASTR:
      return Object.assign({}, state, {
        visible: true,
        config: action.payload
      });
    case HIDE_TOASTR:
      return Object.assign({}, state, {
        visible: false,
        config: action.payload
      });
    default:
      return state;
  }
}
