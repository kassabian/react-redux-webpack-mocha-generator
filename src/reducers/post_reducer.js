import {GET_ALL_POSTS, GET_USER_POSTS, GET_POST_BY_ID, DELETE_POST, ADD_POST} from '../actions/post_actions';

const INITIAL_STATE = {all: [], userPosts: {}, selectedPost: null};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case GET_ALL_POSTS:
      return Object.assign({}, state, {
        all: action.payload.data,
        selectedPost: null
      });
    case GET_USER_POSTS:
      return Object.assign({}, state, {
        userPosts: {
          posts: action.payload.data
        }
      });
    case GET_POST_BY_ID:
      return Object.assign({}, state, {
        selectedPost: action.payload.data
      });
    case ADD_POST:
      let arr = state.all;
      let newPostTemp = JSON.parse(Object.keys(action.payload.data)[0]);
      let newPost = {id: action.payload.data.id, userId: newPostTemp.data.userId, title: newPostTemp.data.title, body: newPostTemp.data.body};
      arr.unshift(newPost);

      return Object.assign({}, state, {
        all: arr
      });
    case DELETE_POST:
      return state;
    default:
      return state;
  }
}
