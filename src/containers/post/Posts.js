import React, { PropTypes } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Link} from 'react-router';

import PostList from '../../components/posts/post_list';

import {getAllPosts} from '../../actions/post_actions';
import {showModal, MODAL_TYPES} from '../../actions/modal_actions';


class Posts extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.props.getAllPosts();
  }

  render () {
    return (
      <section>
        <div className="row add-top">
          <div className="col-xs-11">
            <h3>Posts:</h3>
          </div>
          <div className="col-xs-1">
            <button onClick={() => this.props.showModal(MODAL_TYPES.ADD_POST_FORM)} className="btn btn-sm btn-primary">+</button>
          </div>
          <div className="col-xs-12"><hr /></div>
        </div>
        <div className="row">
          <div className="col-xs-12">
            <PostList posts={(this.props.posts.hasOwnProperty('all')) ? this.props.posts.all : null} />
          </div>
        </div>
      </section>
    )
  }
}

function mapStateToProps(state) {
  return {
    posts: state.posts
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({getAllPosts, showModal}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Posts);
