import React, { PropTypes } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Link} from 'react-router';

import Breadcrumb from '../../components/common/layout/breadcrumb';

import {getPostById, deletePost} from '../../actions/post_actions';
import {showToastr} from '../../actions/ktoastr_actions';

class PostDetails extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.props.getPostById(this.props.params.id);
  }

  deletePost(postId) {
    this.props.deletePost(this.props.params.id).then(() => {
      this.props.showToastr({
        type: 'success',
        title: 'Successfully Deleted!',
        body: 'Blog post has been deleted'
      });
      this.context.router.push('/posts');
    });
  }

  render () {
    let post = (this.props.post) ? this.props.post : null;
    if(!post) {
      return (
        <div className="rwo add-top text-xs-center">
          <div className="col-xs-12">
            <h3>Loading...</h3>
          </div>
        </div>
      );
    }
    return (
      <section>
        <Breadcrumb breadcrumbs={[{route: '/', title: 'Home', active: false}, {route: '/posts', title: 'Posts', active: false}, {title: post.title, active: true}]} />

        <div className="row add-top">
          <div className="col-xs-12 col-sm-10">
            <h2>{post.title}</h2>
            <p>{post.body}</p>
          </div>
          <div className="col-xs-12 col-sm-2">
            <button className="btn btn-danger" onClick={() => this.deletePost(post.id)}>Delete</button>
          </div>
        </div>
      </section>
    );
  } // ***END render()
}

PostDetails.contextTypes = {
  router: PropTypes.object
}

function mapStateToProps(state) {
  return {
    post: state.posts.selectedPost
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({getPostById, deletePost, showToastr}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(PostDetails);
