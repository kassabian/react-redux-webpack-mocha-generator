import React, { PropTypes } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Modal from 'react-modal';

import {hideModal, MODAL_TYPES} from '../../actions/modal_actions';

import AddPostForm from '../../components/posts/add_post_form';

const customStyles = {
  overlay : {
    position          : 'fixed',
    top               : 0,
    left              : 0,
    right             : 0,
    bottom            : 0,
    backgroundColor   : 'rgba(0, 0, 0, 0.65)'
  },
  content : {
    position                   : 'absolute',
    top                        : '70px',
    left                       : '70px',
    right                      : '70px',
    bottom                     : '70px',
    border                     : '1px solid #ccc',
    background                 : '#fff',
    overflow                   : 'auto',
    WebkitOverflowScrolling    : 'touch',
    borderRadius               : '4px',
    outline                    : 'none',
    padding                    : '20px',
    height                    : 'auto',
  }
};

class ModalContainer extends React.Component {
  constructor(props) {
    super(props);
  }

  displayModalContent() {
    switch (this.props.modal.type) {
      case MODAL_TYPES.ADD_POST_FORM:
        return (<AddPostForm />);
      default:
        return null;
    }
  }

  render () {
    if(!this.props.modal.visible) return null;

    return (
      <Modal isOpen={this.props.modal.visible} onRequestClose={() => this.props.hideModal()} closeTimeoutMS={300} shouldCloseOnOverlayClick={true} style={customStyles} >
        {this.displayModalContent()}
      </Modal>
    );
  }
}

function mapStateToProps(state) {
  return {
    modal: state.modal
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({hideModal}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ModalContainer);
