import React, { PropTypes } from 'react';
import {Link} from 'react-router';

class Homepage extends React.Component {
  constructor(props) {
    super(props);
  }

  render () {
    return (
      <div className="add-top">
        <h1>Hello from RiRi React+Redux Starter</h1>
        <Link to="users">
          User Directory
        </Link>
      </div>
    )
  }
}

export default Homepage;
