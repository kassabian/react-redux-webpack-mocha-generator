import React, { PropTypes } from 'react';
import {Link} from 'react-router';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {getUserInfo} from '../../actions/user_actions';
import {getUserPosts} from '../../actions/post_actions';

import Gmaps from '../../components/common/gmaps';
import Breadcrumb from '../../components/common/layout/breadcrumb';
import PostList from '../../components/posts/post_list';

class UserDetail extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.props.getUserInfo(this.props.params.id);
    this.props.getUserPosts(this.props.params.id);
  }

  render () {
    const user = (this.props.selectedUser) ? this.props.selectedUser : null;
    if(!user) {
      return (
        <div className="rwo add-top text-xs-center">
          <div className="col-xs-12">
            <h3>Loading...</h3>
          </div>
        </div>
      );
    }

    return (
      <section>
        <Breadcrumb breadcrumbs={[{route: '/', title: 'Home', active: false}, {route: '/users', title: 'User', active: false}, {title: user.name, active: true}]} />

        <div className="row add-top add-bottom">
          <div className="col-xs-12 col-sm-8">
            <h1>{user.name}</h1>
            <p className="card-text">Email: {user.email}</p>
            <p className="card-text">Phone #: {user.phone}</p>
            <p className="card-text">Website: {user.website}</p>
          </div>
          <div className="col-xs-offset-4 col-xs-4 col-sm-offset-0 col-sm-4" style={{height: "250px"}}>
            <Gmaps lat={parseFloat(user.address.geo.lat)} lng={parseFloat(user.address.geo.lng)} />
          </div>
        </div>

        <div className="row add-top">
          <div className="col-xs-12">
            <h3>Posts:</h3>
            <hr />
          </div>
          <div className="col-xs-12">
            <PostList posts={(this.props.posts.userPosts.hasOwnProperty('posts')) ? this.props.posts.userPosts.posts : null} />
          </div>
        </div>
      </section>
    );
  }
}

function mapStateToProps(state) {
  return {
    selectedUser: state.users.selectedUser,
    posts: state.posts
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({getUserInfo, getUserPosts}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(UserDetail);
