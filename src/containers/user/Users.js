import React, { PropTypes } from 'react';
import {Link} from 'react-router';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {getAllUsers} from '../../actions/user_actions';

class Users extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.props.getAllUsers();
  }

  renderListItems() {
    return this.props.users.all.map(function(u) {
      return (
        <div className="col-xs-6 col-sm-4 add-top" key={u.id}>
          <div className="card">
            <img className="card-img-top img-responsve" src="http://placehold.it/350x150" alt={u.name} width="100%" style={{height: '150px'}}/>
            <div className="card-block">
              <h4 className="card-title">{u.name}</h4>
              <p className="card-text">Email: {u.email}</p>
              <p className="card-text">Phone #: {u.phone}</p>
              <p className="card-text">Website: {u.website}</p>
              <p className="card-text"><Link to={"users/" + u.id}><small className="text-muted">View User</small></Link></p>
            </div>
          </div>
        </div>
      );
    });
  }

  render () {
    if(this.props.users.all.length <= 0) {
      return (
        <div className="row add-top text-xs-center">
          <div className="col-xs-12 text-xs-center">
            <h3>Loading...</h3>
          </div>
        </div>
      );
    }

    return (
      <section className="add-top add-bottom">
        <div className="card-deck-wrapper">
          <div className="card-deck">
            {this.renderListItems()}
          </div>
        </div>
      </section>
    )
  }
}

function mapStateToProps(state) {
  return {
    users: state.users
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({getAllUsers}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Users);
