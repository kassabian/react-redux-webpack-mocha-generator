import React from 'react';
import { Router, Route, IndexRoute } from 'react-router';

import App from '../components/app';
import Homepage from '../containers/Homepage';

import Users from '../containers/user/Users';
import UserDetails from '../containers/user/UserDetails';

import Posts from '../containers/post/Posts';
import PostDetails from '../containers/post/PostDetails';

export default (

  <Route path="/" component={App}>
    <IndexRoute component={Homepage} />
    <Route path="/users">
      <IndexRoute component={Users} />
      <Route path=":id" component={UserDetails} />
    </Route>
    <Route path="/posts">
      <IndexRoute component={Posts} />
      <Route path=":id" component={PostDetails} />
    </Route>
  </Route>

);
