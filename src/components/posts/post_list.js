import React, { PropTypes } from 'react';
import {Link} from 'react-router';

class PostList extends React.Component {
  constructor(props) {
    super(props);
  }

  renderPostItem() {
    if(this.props.posts) {
      const posts = this.props.posts;

      return posts.map(function(p) {
        return (
          <div className="list-group-item" key={p.id}>
            <Link to={`/posts/${p.id}`}>
              <h4 className="list-group-item-heading">{p.title}</h4>
              <p className="list-group-item-text">{p.body}</p>
            </Link>
          </div>
        );
      });
    }
  }

  render () {
    return (
      <section>
        <div className="list-group">
          {this.renderPostItem()}
        </div>

      </section>
    )
  }
}

export default PostList;
