import React, { PropTypes } from 'react';
import { reduxForm } from 'redux-form';
import { Link } from 'react-router';

import {addPost} from '../../actions/post_actions';
import {showToastr} from '../../actions/ktoastr_actions';
import {hideModal} from '../../actions/modal_actions';

class PostsNew extends React.Component {
  constructor(props) {
    super(props);
  }

  onSubmit(props) {
    this.props.addPost(props).then(() => {
      this.props.hideModal();
      this.props.showToastr({
        type: 'success',
        title: 'Post Created',
        body: 'Your new Blog post has been Published'
      });
    });
  }

  render() {
    const { fields: { title, body }, handleSubmit } = this.props;

    return (
      <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
        <h3>Create A New Post</h3>
        <hr />
        <div className={`form-group ${title.touched && title.invalid ? 'has-danger' : ''}`}>
          <label>Title</label>
          <input type="text" className="form-control" {...title} />
          <div className="text-help">
            {title.touched ? title.error : ''}
          </div>
        </div>

        <div className={`form-group ${body.touched && body.invalid ? 'has-danger' : ''}`}>
          <label>body</label>
          <textarea className="form-control" placeholder="..." {...body} />
          <div className="text-help">
            {body.touched ? body.error : ''}
          </div>
        </div>

        <button type="submit" className="btn btn-primary pull-xs-right">Creatae Post &rarr;</button>
      </form>
    );
  }
}

PostsNew.contextTypes = {
  router: PropTypes.object
};

function validate(values) {
  let errors = {};

  if (!values.title) {
    errors.title = 'Enter a Post Title';
  }
  if(!values.body) {
    errors.body = 'Enter Post Content';
  }

  return errors;
}

// connect: first argument is mapStateToProps, 2nd is mapDispatchToProps
// reduxForm: 1st is form config, 2nd is mapStateToProps, 3rd is mapDispatchToProps
export default reduxForm({
  form: 'PostsNewForm',
  fields: ['title', 'body'],
  validate
}, null, { addPost, showToastr, hideModal })(PostsNew);
