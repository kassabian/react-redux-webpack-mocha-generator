import React, { PropTypes } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import {hideToastr} from '../../actions/ktoastr_actions';

class Ktoastr extends React.Component {
  constructor(props) {
    super(props);
  }

  renderToastr() {
    if(this.props.ktoastr.visible) {
      setTimeout(function() {
        this.props.hideToastr();
      }.bind(this), 3500)
    }
    return (
      <div className="toastr-wrapper" key="ktoastr">
        <div className={`card card-inverse card-${this.props.ktoastr.config.type} text-xs-center`}>
          <div className="card-block">
            <blockquote className="card-blockquote">
              <p>{this.props.ktoastr.config.title}</p>
              <footer>{this.props.ktoastr.config.body}</footer>
            </blockquote>
          </div>
        </div>
      </div>
    );
  }

  render () {
    if(this.props.ktoastr.visible) {
      return (
        <ReactCSSTransitionGroup transitionName="ktoastr" transitionAppear={true} transitionEnterTimeout={1200} transitionAppearTimeout={3500} transitionLeaveTimeout={500}>
          {this.renderToastr()}
        </ReactCSSTransitionGroup>
      );
    } else {
      return null;
    }
  } // ***END render()
}

function mapStateToProps(state) {
  return {
    ktoastr: state.ktoastr
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({hideToastr}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Ktoastr);
