import React, { PropTypes } from 'react';
import {Link} from 'react-router';

class Breadcrumb extends React.Component {
  constructor(props) {
    super(props);
  }
  breadcrumbItems() {
    return this.props.breadcrumbs.map(function(b) {
      return (
        <li key={b.title} className={(b.active) ? 'active' : null}>
          {(!b.active) ?
            <Link to={b.route}>{b.title}</Link>
          :
            b.title
          }
        </li>
      );
    });
  }
  render () {
    return (
      <div className="row add-top">
        <div className="col-xs-12">
          <ol className="breadcrumb">
            {this.breadcrumbItems()}
          </ol>
        </div>
      </div>
    );
  }
}

export default Breadcrumb;
