import 'stylesheets/modules/nav';

import React, { PropTypes } from 'react';
import {Link, IndexLink} from 'react-router';

class Nav extends React.Component{
  render () {
    return (
      <nav className="navbar navbar-full navbar-dark bg-primary">
        <a className="navbar-brand" href="/">RiRi (React+Redux)</a>
        <ul className="nav navbar-nav">
          <li className="nav-item">
            <IndexLink to="/" className="nav-link" activeClassName="active">Home</IndexLink>
          </li>
          <li className="nav-item">
            <Link to="/users" className="nav-link" activeClassName="active">Users</Link>
          </li>
          <li className="nav-item">
            <Link to="/posts" className="nav-link" activeClassName="active">Posts</Link>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="#">About</a>
          </li>
        </ul>
      </nav>
    )
  };
}

export default Nav;
