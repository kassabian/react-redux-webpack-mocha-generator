import React, { PropTypes } from 'react';
import { GoogleMapLoader, GoogleMap } from 'react-google-maps';

class Gmaps extends React.Component {
  constructor(props) {
    super(props);
  }

  render () {
    return (
      <GoogleMapLoader
            containerElement={ <div style={{width: '100%', height: '100%'}} /> }
            googleMapElement={
              <GoogleMap defaultZoom={4} defaultCenter={{lat: this.props.lat, lng: this.props.lng}} />
            }
      />
    )
  }
}

export default Gmaps;
