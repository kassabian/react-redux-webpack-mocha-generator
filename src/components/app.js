import React, { PropTypes } from 'react';

import Nav from './common/layout/nav';
import Ktoastr from './common/ktoastr';
import ModalContainer from '../containers/util/modal_container';

class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render () {
    return (
      <div>
        <Nav />
        <div className="container">
          {this.props.children}
        </div>

        <ModalContainer />
        <Ktoastr />
      </div>
    )
  }
}

export default App;
